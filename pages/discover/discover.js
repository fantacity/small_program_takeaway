var api = require('../../utils/api.js')
var amapFile = require('../../utils/amap-wx.js');
var markersData = {
  latitude: '',//纬度
  longitude: '',//经度
  key: "84041ff64baa654da90d58ffbe2a5f13"//申请的高德地图key
};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 3000,
    duration: 800,
    ball:[],
    ballBackgroundImg:'',
    banner:[],
    act2:[],
    act3:[],
    act4:[],
    weather:[],
    loadCity:'获取位置'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.request({
      url: api.jindongUrl,
      header: {
        'content-type': 'application/json'
      },
      method: "GET",
      complete: function (res) {
        for (var i = 0; i < res.data.result.data.length; i++) {
          if (res.data.result.data[i].floorStyle==='ball'){
            that.setData({
              ball: res.data.result.data[i].data,
              ballBackgroundImg: res.data.result.data[i].borderImg||''
            });
          } else if (res.data.result.data[i].floorStyle === 'banner'){
            that.setData({
              banner: res.data.result.data[i].data
            });
          } else if (res.data.result.data[i].floorStyle === 'act2') {
            that.setData({
              act2: res.data.result.data[i].data
            });
          } else if (res.data.result.data[i].floorStyle === 'act3') {
            that.setData({
              act3: res.data.result.data[i].data
            });
          }else if (res.data.result.data[i].floorStyle === 'act4') {
            that.setData({
              act4: res.data.result.data[i].data
            });
          }
        }
        if (res == null || res.data == null) {
          console.error('网络请求失败');
          return;
        }
      }
    })
  },
  bindgetLocation:function(){
    var that = this;
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success: function (res) {
        var latitude = res.latitude//维度
        var longitude = res.longitude//经度
        that.loadCity(latitude, longitude);
      }
    })
  },
  loadCity: function (latitude, longitude) {
    var that = this;
    var myAmapFun = new amapFile.AMapWX({ key: markersData.key });
    myAmapFun.getRegeo({
      location: '' + longitude + ',' + latitude + '',//location的格式为'经度,纬度'
      success: function (data) {
        //城市详情
        that.setData({
          loadCity: data[0].regeocodeData.addressComponent.city + '/' + data[0].regeocodeData.addressComponent.district
        });
        wx.showModal({
          content: '当前城市为:'+data[0].name,
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        });
      },
      fail: function (info) { }
    });
    //获取天气
    myAmapFun.getWeather({
      success: function (data) {
        that.setData({
          weather: data
        });
        //成功回调
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    })
  },
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      autoplay: true
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      autoplay: false
    });
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})