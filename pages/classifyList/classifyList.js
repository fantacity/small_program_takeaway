var api = require('../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    text:'',
    foodList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      text: options.id,
    });
    var that = this
    wx.request({
      url: api.datalist().recommend,
      header: {
        'content-type': 'application/json'
      },
      method: "GET",
      complete: function (res) {
        for (var i = 0; i < res.data.data.length; i++) {
          res.data.data[i].imgurl = res.data.data[i].imgurl.replace(/w.h/g, '150.0')
        }
        that.setData({
          foodList: res.data.data
        });
        if (res == null || res.data == null) {
          console.error('网络请求失败');
          return;
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})